defmodule DistNeatEx.Mixfile do
  use Mix.Project

  def project do
    [
      app: :dist_neat_ex,
      version: "1.0.0",
      elixir: "~> 1.5",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "DistNeatEx",
      source_url: "https://gitlab.com/onnoowl/dist_neat_ex",
      docs: [main: DistNeatEx]
    ]
  end

  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [
      extra_applications: [:logger],
      mod: {DistNeatEx.Application, []}
    ]
  end

  defp deps do
    [
      {:neat_ex, "~> 1.3.0"},
      {:ex_doc, "~> 0.14", only: :dev, runtime: false}
    ]
  end

  defp description do
    """
    This project allows for Distributed Neuroevolution of Augmenting Topologies. Evolve a population of artificial neural networks on a distributed cluster of devices, using the NEAT algorithm.
    """
  end

  defp package do
    [
      maintainers: ["Stuart Hunt"],
      licenses: ["MIT License"],
      links: %{"GitLab" => "https://gitlab.com/onnoowl/dist_neat_ex"}
    ]
  end
end
