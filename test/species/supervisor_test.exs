defmodule DistNeatEx.Species.SupervisorTest do
  use ExUnit.Case
  alias DistNeatEx.Species

  test "Can start a child in the dynamic supervisor" do
    # Species.Supervisor.start_species()
  end

  test "Species restarted with same ref" do
    {:ok, ref1} = Species.Supervisor.start_species()
    pid1 = GenServer.whereis({:global, ref1})
    Process.exit(pid1, :kill)
    Process.sleep(100)
    [{_, pid2, _, _}] = Supervisor.which_children({:global, {Species.Supervisor, Node.self(), :undef_run}})
    ref2 = GenServer.call(pid2, :get_ref)

    assert pid1 != pid2
    assert ref1 == ref2
  end

  test "Start is synchronous" do
    refs = for _ <- 1..1000 do
      {:ok, ref} = Species.Supervisor.start_species()
      ^ref = GenServer.call({:global, ref}, :get_ref)
    end
    for ref <- refs do
      pid = GenServer.whereis({:global, ref})
      Supervisor.terminate_child({:global, {Species.Supervisor, Node.self(), :undef_run}}, pid)
    end
  end
end