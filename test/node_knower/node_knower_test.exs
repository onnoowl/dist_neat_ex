defmodule DistNeatEx.NodeKnowerTest do
  use ExUnit.Case
  alias DistNeatEx.NodeKnower

  test "Can update KnodeKnower with species knowledge" do
    ref = make_ref()
    run = :run1
    sk = NodeKnower.SpeciesKnowledge.new(node_name: Node.self(), run_id_term: run, representative: nil, avg_fitness: 0, generation: 0)
    NodeKnower.update_species(ref, sk)

    state = NodeKnower.call(:get_state)
    assert MapSet.member?(state.nodes[Node.self()][run], ref)
    assert state.species[ref] == sk

    fakenode = :zfakenode@nohost
    send(NodeKnower, {:nodeup, fakenode})

    sk = %{sk | node_name: fakenode}
    NodeKnower.update_species(ref, sk)

    state = NodeKnower.call(:get_state)
    refute MapSet.member?(state.nodes[Node.self()][run], ref)
    assert MapSet.member?(state.nodes[fakenode][run], ref)

    send(NodeKnower, {:nodedown, fakenode})

    state = NodeKnower.call(:get_state)
    refute state.nodes[fakenode]
    refute state.species[ref]
  end

  test "Species down removes it from node knower" do
    refs = Stream.repeatedly(&make_ref/0) |> Enum.take(5)
    run = :run1
    sk_args = [node_name: Node.self(), run_id_term: run, representative: nil, avg_fitness: 0, generation: 0]
    sk = NodeKnower.SpeciesKnowledge.new(sk_args)

    for ref <- refs do
      NodeKnower.update_species(ref, sk)
    end

    for ref <- refs do
      state = NodeKnower.call(:get_state)
      assert MapSet.member?(state.nodes[Node.self()][run], ref)
      assert state.species[ref] == sk
    end

    ref = hd(refs)
    NodeKnower.species_down(ref, sk_args, nil)
    state = NodeKnower.call(:get_state)
    refute MapSet.member?(state.nodes[Node.self()][run], ref)
    refute state.species[ref] == sk
  end
end