# Design

Each node should have the following processes:


OTP Modules:
	- NeatNode.Application
	- NeatNode.Supervisor
		- BackupBuddy
			- has just it's backup buddy's data, and will redestribute it if he dies
			- uses Node.monitor on backup buddy
			- always backs up the next alphabetical Node
		- Species.Supervisor (:simple\_one\_for\_one)
			- Species (gen\_server)
				# - min/max generation number. When breeding with another species, updates the min and max
				- population

	- NodeKnower.Supervisor #Always exists on the alphabetically first Node. Maybe use System.schedulers\_online() for machine with most cores.
		- NodeKnower #It's functions will auto-launch it on an agreed machine if it doesn't exist.
			- has cross-node knowledge
			- state structure %{nodes: %{\_node\_name: %{run\_id\_term => %{species: %{\_ref: %{representative: \_, avg\_fitness: \_, generation: \_}, ...}}}, ...} }
			- num\_species(node\_name)
			- sum\_fitness(node\_name) #sum of average fitnesses
			- avg\_fitness(node\_name) #sum\_fitness / num\_species
			- make sure to have auto shutdown feature if it is no longer used/first


Non-OTP Modules:
	- Species.Distributor
	- DistNeatEx


Robustness checklist:
	- Consider any node can crash at any time.
	- Consider if multiple machines try to start KnowledgeCache at once.
	- Scutinize the backup buddy switching process
	- Consider if a single node disconnects from the rest, what it's recovery stuff will try to do


Designing this make sure to consider new species and species redistribution from backups. Backups is why the knowledge stuff needs to be synchronous, so that many can be added in sequence.
On add species (should be synchronous):
	1. Pick node with least number of species, breaking ties by picking node with lowest avg\_fitness.
		- Maybe instead just pick the lowest sum\_fitness
	2. Update knowledge cache
	3. Create species gen\_server on target node.
		- Maybe the gen\_server should update the knowledge\_cache itself? in the init?

<!-- For setting knowledge:
	1. Synchronously update our knowledge\_cache
	2. Asynchronously update all other knowledge\_caches -->

For cross species mating, should probably request information in batches. A species should figure its shit out, and send one large request.

Species size management done per node



# Q&A and notes

- ? Deployment?
- Use edeliver for deployment. Hopefully wait for their docker support.

- ? Should each Species be a GenServer? Or should each Node just be a neat\_ex instance, and then add some cross-polination?
- Species are maintained over time, so each species should be GenServer.

- ? Does edeliver autoconnect nodes? (if it doesn't, might be able to use PRODUCTION\_HOSTS env var to work it out. May not be available on prod nodes)
- Use Docker/Rancher to auto-discover nodes. There are guides.



# neat\_ex todo items

- I think neat\_ex right now keeps the same species rep. According to the paper, the species rep should be a new random member of the population each and every time.

- Use tuples instead of unique number ID system


# TODO (10/26)

NodeKnower and Species state recovery with supervisor restart
NodeKnower recovery (and reconstruction)
Backup buddies
Do something with species health (timestamp) information