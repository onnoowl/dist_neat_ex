defmodule DistNeatEx do
  @moduledoc """
  Documentation for DistNeatEx.

  Configuration:
  Ensure the config has a node list to connect to. It will then auto connect to those nodes.
      config :dist_neat_ex, nodes: [
        :node1name@127.0.0.1,
        :node2name@?.?.?.?,
        ...
      ]

  For example usage and other information, see the [README.md](https://gitlab.com/onnoowl/dist_neat_ex).

  Below are the evolution options, their defaults, and descriptions.

  | Paramater                   | Default    | Description |
  | --------------------------- | ---------- | ----------- |
  | :run_id or :name or :run_id_term | make_ref() | The term for the name of the run. Defaults to a random ref. |
  | :population_size            | 20         | The population size PER CPU/CORE, so a four core machine will have 4*X members. |
  | :compatibility_threshold    | 2.0        | Initial threshold used for speciation. Decrease for more species, increase for fewer. |
  | :survival_ratio             | 0.2        | Ratio of the population that persists each generation. |
  | :dropoff_age                | 15         | If a species does not progress for this many generations, it dies. |
  | :structure_diff_coefficient | 2.0        | Coefficient for how much structure differences matter in speciation. |
  | :weight_diff_coefficient    | 1.0        | Coefficient for how much weight differences matter in speciation. |
  | :weight_range_min           | -1.0       | Minimum for initial weight creation. |
  | :weight_range_max           | 1.0        | Maximum for initial weight creation. |
  | :weight_mutation_power      | 1/6        | Standard deviation for the random modifiers used to mutate weights. |
  | :new_weight_chance          | 0.1        | Ratio of weight mutations that get assigned totally new values. |
  | :child_from_mutation_chance | 0.25       | Ratio of children that result from mutation (not mating). |
  | :interspecies_mating_chance | 0.01       | Ratio of children that result from interspecies mating. |
  | :new_node_chance            | 0.03       | Ratio of mutations that add a new node (the rest mutate weights). |
  | :new_link_chance            | 0.05       | Ratio of mutations that add a new link (the rest mutate weights). |
  | :recurrent_nodes            | true       | Whether or not recurrent nodes are allowed. |
  """
  alias DistNeatEx.{NodeKnower, Species}

  @doc """
  Begin a distributed evolution run with a given seed, fitness function, and options. Returns the run_id.

  See the moduledoc for options.

  The seed ANN used to generate the initial population. This is used for specifying the input/output neurons that all ANNs should have, as well as any priliminary topology ANNs should start with.

  The fitness function should take an ANN, and return a positive fitness value, where larger numbers mean preferable behavior.
  """
  def evolve(seed, fitness_function, opts_keyword_list \\ []) do
    run_id_term = opts_keyword_list[:name] || opts_keyword_list[:run_id] || opts_keyword_list[:run_id_term] || make_ref()
    opts_keyword_list = opts_keyword_list
    |> Keyword.delete(:name)
    |> Keyword.delete(:run_id)
    |> Keyword.delete(:run_id_term)
    |> Keyword.put_new(:population_size, 20)
    |> Keyword.put_new(:compatibility_threshold, 2.0)

    neat_opts = Neat.Options.new(seed, fitness_function, true, opts_keyword_list)

    DistNeatEx.Helper.init_run(run_id_term, neat_opts)
  end

  @doc "Shuts down a run given its run_id."
  def stop(run_id_term) do
    for node <- NodeKnower.all_nodes() do
      pid = GenServer.whereis({:global, {DistNeatEx.Species.Supervisor, node, run_id_term}})
      if pid, do: Supervisor.stop(pid)
    end
    NodeKnower.cast({:delete_run, run_id_term})
  end

  @doc "Returns a list of all active runs."
  def list() do
    NodeKnower.call(:get_state) |> NodeKnower.get_run_ids()
  end

  @doc "Returns a map with information on all active runs. run_ids are the keys, and the values are a map with keys :number_species, :fitness, :gen, and :species_count_per_node."
  def info() do
    state = NodeKnower.call(:get_state)
    Enum.reduce list(), %{}, fn run_id, map ->
      info = %{
        number_species: NodeKnower.cross_node_num_species(state, run_id),
        fitness: NodeKnower.cross_node_best_ann(state, run_id) |> elem(1) |> elem(1) |> Float.round(3),
        gen: NodeKnower.cross_node_max_by_key(state, run_id, :generation) |> elem(1),
        species_count_per_node: state.nodes |> Enum.map(fn {n, map} -> {n, MapSet.size(map[run_id] || MapSet.new())} end)
      }
      Map.put(map, run_id, info)
    end
  end

  @doc "Pretty prints info()."
  def print_info(width \\ 71) do
    IO.inspect(info(), syntax_colors: [number: :red, atom: :blue], width: width)
    :ok
  end

  @doc "Starts a loop (that can be killed by pressing enter) that prints out the info()."
  def loop_info(interval \\ 250) do
    pid = spawn(DistNeatEx.Helper, :loop_info, [interval])
    IO.gets("")
    Process.exit(pid, :kill)
    :ok
  end

  @doc "Returns {ann, fitness} for the best ann of a given run."
  def best_ann(run_id) do
    DistNeatEx.NodeKnower.best_ann(run_id)
  end

  defmodule Helper do
    def init_run(run_id_term, neat_opts) do
      seed = neat_opts.seed
      {new_members, fun} = if Map.size(seed.connections) == 0 do
        {[], &Neat.Reproduction.mutate_new_link/2}
      else
        {[seed], &Neat.Reproduction.mutate/2}
      end
      new_members = Enum.map (length new_members)..(neat_opts.population_size - 1), fn _ ->
        fun.(seed, neat_opts)
      end
      species = Enum.reduce new_members, [], fn member, species ->
        Neat.Speciation.speciateMember(member, species, neat_opts)
      end
      #species has the structure [{rep={ann, 0, {0, 0}}, members=[ann, ...]}, ...]
      for {{rep, _, {_, _}}, members} <- species do
        node = NodeKnower.most_available_node()
        Species.Supervisor.start_species(%{neat_opts: neat_opts, run_id_term: run_id_term, members: members, generation: 0, representative: rep}, node)
      end

      run_id_term
    end

    def loop_info(interval) do
      DistNeatEx.print_info()
      Process.sleep(interval)
      DistNeatEx.Helper.loop_info(interval)
    end
  end
end
