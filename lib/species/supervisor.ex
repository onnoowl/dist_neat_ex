defmodule DistNeatEx.Species.Supervisor do
  use Supervisor
  alias DistNeatEx.Species

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, nil, opts)
  end

  def init(_args) do
    species_spec = Supervisor.child_spec(Species, start: {Species, :start_link, []}, restart: :transient)

    # We start a supervisor with a simple one for one strategy.
    # The agent won't be started now but later on.
    Supervisor.init([species_spec], strategy: :simple_one_for_one)
  end

  def start_species(args \\ %{neat_opts: Neat.Options.new(nil, nil, true), members: [], run_id_term: :undef_run, representative: :none}, node \\ Node.self()) do
    if Node.self() != node do
      :rpc.call(node, __MODULE__, :start_species, [args, node])
    else
      ref = make_ref()
      name = {:global, {__MODULE__, node, args.run_id_term}}
      pid = GenServer.whereis(name)
      {:ok, pid} = if pid && Process.alive?(pid) do
        {:ok, pid}
      else
        case DistNeatEx.Species.SuperDupervisor.start_supervisor(node, args.run_id_term) do
          {:ok, pid} -> {:ok, pid}
          {:error, {:already_started, pid}} -> {:ok, pid}
          {:error, :already_present} -> {:ok, GenServer.whereis(name)}
        end
      end
      with {:ok, _pid} <- Supervisor.start_child(pid, [args, [name: {:global, ref}]]) do
        {:ok, ref}
      end
    end
  end
end