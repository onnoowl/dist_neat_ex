defmodule DistNeatEx.Species.SuperDupervisor do
  use Supervisor
  alias DistNeatEx.Species

  @moduledoc """
  This is the dynamic supervisor for the DistNeatEx.Species.Supervisor. A species supervisor is launched for each node, and for each run. The run_id_term is part of its name.
  """

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_args) do
    child_spec = Supervisor.child_spec(Species.Supervisor, start: {Species.Supervisor, :start_link, []}, restart: :transient)

    # We start a supervisor with a simple one for one strategy.
    # The agent won't be started now but later on.
    Supervisor.init([child_spec], strategy: :simple_one_for_one)
  end

  def start_supervisor(node, run_id) do
    Supervisor.start_child({__MODULE__, node}, [[name: {:global, {DistNeatEx.Species.Supervisor, node, run_id}}]])
  end
end