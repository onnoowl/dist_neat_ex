defmodule DistNeatEx.Species do
  use GenServer
  alias Neat.{Fitness, PopulationManagement, Reproduction, Speciation, Utils}
  alias DistNeatEx.{Species, NodeKnower}
  defstruct ref: nil, run_id_term: nil, neat_opts: nil, members: [], representative: nil, generation: 0, avg_fitness: 0, max_fitness: 0, time_since_last_improvement: 0, best_ann: {nil, 0.0}, node_knower: nil, child_buffer: [], mates_buffer: [], status: :runnable, species_age: 0

  def start_link(args, opts) do
    GenServer.start_link(__MODULE__, [args, opts], opts)
  end

  def init([args, opts]) do
    {:global, ref} = opts[:name]
    # IO.puts("==============START============== ref: #{inspect ref}")
    status = if length(args.members) > 0 do
      run(ref)
      :running
    else
      :runnable
    end
    state = %Species{
      ref: ref, run_id_term: args.run_id_term, neat_opts: args.neat_opts,
      members: args.members, status: status, generation: args[:generation] || 0,
      representative: args.representative || Enum.random(args.members || [nil])
    }
    |> map_fitness
    |> assign_avg_fitness #we do this prematurely, to make sure we get our starting avg fitness in the NodeKnower before starting.

    NodeKnower.species_up(ref, get_sk_args(state), args.neat_opts) #sync send an update to the real one
    {:ok, state}
  end

  def run(ref), do: GenServer.cast({:global, ref}, :step_evolution)

  def handle_call(:get_ref, _from, state) do
    {:reply, state.ref, state}
  end

  @doc "Steps the evolution process forward one step, and then recursively calls itself."
  def handle_cast(:step_evolution, state) do
    NodeKnower.send_me_state()
    state = state
    |> set_is_running     # Set our state to :running
    |> map_fitness        # Compute the fitness for each neural network
    |> assign_avg_fitness # Compute the average fitness of this species
    |> kill_lessers       # Removes the lower portion of the species

    if should_die?(state) do # Check if we haven't improved for a while
      NodeKnower.species_down(state.ref, get_sk_args(state), state.neat_opts)
      # Notify the node knower we are down, and then stop gracefully
      {:stop, :normal, state}
    else
      state = state
      |> store_best_ann                       # Store the best neural network
      |> randomize_rep                        # Select a new random representative
      |> receive_node_knower_state            # Receive the state of the node knower
      |> repopulate                           # Repopulate
      |> speciate                             # Find a species for each new neural network
      |> Map.update!(:generation,  &(&1 + 1)) # Increment the generation counter
      |> Map.update!(:species_age, &(&1 + 1)) # Increment the species age counter
      |> update_node_knower                   # Send our new information to the node knower

      GenServer.cast(self(), :step_evolution) # Send a :step_evolution message, to loop

      {:noreply, state}
    end
  end

  def handle_cast({:interspecies_mate, ann}, state) do
    {:noreply, update_in(state.mates_buffer, &[ann | &1])}
  end

  def handle_cast({:add_members, members}, state) do
    state = update_in(state.members, &(members ++ &1))
    state = if state.status == :runnable do
      run(state.ref)
      set_is_running(state)
    else
      state
    end
    {:noreply, state}
  end

  def handle_cast({:new_compatibility_threshold, thresh}, state) do
    {:noreply, put_in(state.neat_opts.compatibility_threshold, thresh)}
  end

  defp set_is_running(state), do: %{state | status: :running}

  defp map_fitness(state) do
    update_in(state.members, &Fitness.members_map_fitness(&1, state.neat_opts))
  end

  defp assign_avg_fitness(state=%Species{members: []}), do: state
  defp assign_avg_fitness(state=%Species{members: members, max_fitness: old_max, time_since_last_improvement: tsi}) do
    fitnesses = Enum.map(members, fn {_ann, fitness} -> fitness end)
    max = Enum.max(fitnesses)
    avg = Enum.sum(fitnesses) / length(members)
    tsi = if max == old_max, do: tsi + 1, else: 0
    %{state | avg_fitness: avg, max_fitness: max, time_since_last_improvement: tsi}
  end

  defp should_die?(state) do
    state.members == []
    or (
      (dropoff=state.neat_opts.dropoff_age) not in [-1, :infinity]
      && dropoff < state.time_since_last_improvement
      && state.node_knower
      && NodeKnower.can_i_die?(state.run_id_term) #TODO: make this a single operation, where it checks if it can die, and then dies on node_knower.
    )
    #TODO maybe make it so that species only die off if their population count is too low for X number of evolution steps. This way if there's latency in sending children to this species from other species, it will survive in the meantime.
  end

  defp kill_lessers(state) do
    new_members = PopulationManagement.members_kill_lessers(state.members, state.neat_opts)
    if length(new_members) < 2 and state.species_age < 2 do
      state
    else
      %{state | members: new_members}
    end
  end

  defp store_best_ann(state) do
    %{state | best_ann: hd(state.members)}
  end

  defp randomize_rep(state) do
    %{state | representative: Enum.random(state.members)}
  end

  defp receive_node_knower_state(state) do
    knower = receive do
      {:node_knower_state, state} -> state
    after
      1000 ->
        state.node_knower
    end
    %{state | node_knower: knower}
  end

  #TODO !!! Why do they go extinct when I launch 2 tasks?
  defp repopulate(state=%Species{members: members, neat_opts: opts, avg_fitness: avg_fitness, node_knower: knower}) do
    population_size = opts.population_size * System.schedulers_online()
    avg_fitness_sum = NodeKnower.node_sum_fitness(knower, Node.self(), state.run_id_term)
    # IO.puts("#{Node.self()} #{inspect self()} #{avg_fitness_sum} #{inspect opts.compatibility_threshold}")
    #TODO debug "avg_fitness_sum != 0", ideally this sum should never be 0...
    reproduce_times = if is_number(avg_fitness) && is_number(avg_fitness_sum) && avg_fitness_sum != 0 do
      # IO.puts("Target: #{trunc(Float.round((avg_fitness / avg_fitness_sum) * population_size))}, current: #{length(members)}")
      trunc(Float.round((avg_fitness / avg_fitness_sum) * population_size)) - length(members)
    else
      population_size - length(members)
    end
    children = if reproduce_times > 0 do
      Enum.map(1..reproduce_times, fn _ ->
        ann = Enum.random(members)
        case Reproduction.get_rand_repopulation_type(opts) do
          :interspecies_mating ->
            ref = NodeKnower.get_species_of_run(knower, state.run_id_term) |> Enum.random
            GenServer.cast({:global, ref}, {:interspecies_mate, ann})
            nil
          :mutation ->
            Reproduction.mutate(Utils.deconstruct(ann), opts)
          :mating ->
            Reproduction.breed(ann, Enum.random(members), opts)
        end
      end) ++ Enum.map(state.mates_buffer, fn ann ->
        [mate] = Fitness.members_map_fitness([ann], opts)
        Reproduction.breed(mate, Enum.random(members), opts)
      end)
    else
      []
    end
    |> Enum.filter(&(&1))

    state = update_in(state.child_buffer, &(&1 ++ children))
    %{state | mates_buffer: []}
  end

  defp speciate(state=%Species{child_buffer: children, node_knower: knower, neat_opts: opts, run_id_term: run_id, generation: gen}) do
    {children, knower} = Enum.map_reduce(children, knower, fn child, knower ->
      species = find_species(state, child)
      if species do
        {ref, _} = species
        {{ref, child}, knower}
      else
        node = NodeKnower.most_available_node(knower)
        case Species.Supervisor.start_species(%{neat_opts: opts, run_id_term: run_id, representative: child, members: [], generation: gen}, node) do
          {:ok, ref} ->
            sk_args = [node_name: node, run_id_term: run_id, representative: child, avg_fitness: 0, generation: gen]
            knower = NodeKnower.update_species_helper(knower, ref, sk_args) #update our local copy
            {{ref, child}, knower}
          {:badrpc, :nodedown} -> {nil, knower}
          {:badrpc, {:EXIT, {:normal, _}}} -> {nil, knower}
        end
      end
    end)

    children
    |> Enum.filter(&(&1))
    |> Enum.group_by(&elem(&1, 0), &elem(&1, 1))
    |> Enum.each(fn {ref, children} ->
      GenServer.cast({:global, ref}, {:add_members, children})
    end)

    %{state | child_buffer: [], node_knower: knower}
  end
  defp find_species(state, ann) do
    NodeKnower.get_species_of_run(state.node_knower, state.run_id_term)
    |> Enum.map(fn ref -> {ref, Map.fetch!(state.node_knower.species, ref)} end)
    |> Enum.shuffle
    |> Enum.find(fn {_ref, species} ->
      Speciation.compatibility(state.neat_opts, species.representative, ann) <= state.neat_opts.compatibility_threshold
      #TODO get rid of compatibility_threshold broadcasting... its included in the node_knower download anyways.
    end)
  end

  defp update_node_knower(state) do
    NodeKnower.update_species(state.ref, get_sk_args(state))
    state
  end

  defp get_sk_args(%Species{run_id_term: term, representative: rep, avg_fitness: avg, best_ann: best_ann, generation: gen}) do
    [node_name: Node.self(), run_id_term: term, representative: rep, avg_fitness: avg, best_ann: best_ann, generation: gen]
  end

  def terminate(_reason, state) do
    NodeKnower.species_down(state.ref, get_sk_args(state), nil)
  end
end