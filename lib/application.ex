defmodule DistNeatEx.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  def start(_type, _args) do
    Application.get_env(:dist_neat_ex, :nodes, [])
    |> Enum.map(&Node.connect/1)

    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: DistNeatEx.Worker.start_link(arg)
      DistNeatEx.BackupBuddy,
      DistNeatEx.Species.SuperDupervisor,
      DistNeatEx.NodeKnower
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DistNeatEx.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
