defmodule DistNeatEx.Examples.Xor do
  @moduledoc "Code for evolving neural networks to reproduce the behavior of the XOR logic gates (only with 0's repalced by -1's)."
  alias Ann.Simulation

  def run(run_id \\ nil) do
    run_id = DistNeatEx.evolve(
      Ann.new([:in1, :in2, :bias], [:out]),
      &xor_fitness_function/1,
      compatibility_threshold: 1.8,
      population_size: 18, #per core,
      run_id: run_id
    )
    IO.puts("Run started: #{inspect run_id}")
    run_id
  end

  def xor_fitness_function({_ann, fitness}), do: fitness
  def xor_fitness_function(ann) do
    sim = Simulation.new(ann)
    error = Enum.reduce dataset(), 0, fn {{in1, in2}, out}, error ->
      result = Map.get(Simulation.eval(sim, %{:in1=>in1, :in2=>in2, :bias=>1.0}).data, :out, 0)
      error + abs(result - out)
    end
    :math.pow(8 - error, 2)
  end

  def dataset do
    [{{-1, -1}, -1}, {{1, -1}, 1}, {{-1, 1}, 1}, {{1, 1}, -1}]
  end
end
