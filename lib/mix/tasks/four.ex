defmodule Mix.Tasks.Four do
  @app_dir_file "/tmp/dist_neat_ex_app_dir"

  def run(_) do
    File.write!(@app_dir_file, System.cwd!())

    windows = ["up_left", "up_right", "lower_left", "lower_right"]

    Enum.each(windows, fn window ->
      System.cmd("open", ["-a", "Terminal", "#{:code.priv_dir(:dist_neat_ex)}/#{window}.sh"])
    end)

    :timer.sleep(5000)

    files = Enum.map(windows, fn window -> "/tmp/#{window}.pid" end)
    pids = Enum.map(files, fn window -> System.cmd("cat", [window]) end) |> Enum.map(&elem(&1, 0))

    IO.gets("Press ENTER to quit")

    pids |> Enum.map(&System.cmd("kill", ["-9", &1]))
    files |> Enum.map(&File.rm/1)
    File.rm!(@app_dir_file)
  end
end
