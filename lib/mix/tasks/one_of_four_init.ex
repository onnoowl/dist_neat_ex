defmodule Mix.Tasks.OneOfFourInit do
  def run([name]) do
    File.write!("/tmp/#{name}.pid", :os.getpid())
    IEx.Helpers.clear()
    Application.ensure_all_started(:dist_neat_ex)
    name |> String.split("_") |> Enum.map(&String.capitalize/1) |> Enum.join(" ") |> IO.puts

    ensure_started_on_all_nodes()

    # cond do
    #   name == "up_right" ->
    #     spawn(fn -> apply(DistNeatEx.Examples.Xor, :run, [:a]) end)
    #   name == "up_left" ->
    #     spawn(fn -> apply(DistNeatEx.Examples.Xor, :run, [:b]) end)
    #   name == "lower_right" ->
    #     spawn(fn -> apply(DistNeatEx, :loop_info, []) end)
    #   :else ->
    #     :ok
    # end
  end

  defp ensure_started_on_all_nodes() do
    for node <- Application.get_env(:dist_neat_ex, :nodes, []) do
      loop_until_started(node)
    end
  end

  defp loop_until_started(node) do
    case :rpc.call(node, Application, :ensure_all_started, [:dist_neat_ex]) do
      {:error, {:dist_neat_ex, _}} ->
        Process.sleep(100)
        loop_until_started(node)
      {:ok, _} -> :ok
    end
  end
end
