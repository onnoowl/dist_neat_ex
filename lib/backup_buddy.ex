defmodule DistNeatEx.BackupBuddy do
  use GenServer

  def start_link(arg \\ %{}, opts \\ [name: __MODULE__]) do
    GenServer.start_link(__MODULE__, arg, opts)
  end

  def init(arg) do
    {:ok, arg}
  end
end