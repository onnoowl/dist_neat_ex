# DistNeatEx

**TODO: Add description**

## Installation

The package can be installed
by adding `dist_neat_ex` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:dist_neat_ex, "~> 1.0.0"}]
end
```

## Documentation

For details, the latest documentation can be found at [https://hexdocs.pm/dist_neat_ex](https://hexdocs.pm/dist_neat_ex). For example usage, see the example below.

## DistNeatEx Example Usage

Here's a simple example that shows how to setup an evolution that evolves neural networks to act like binary XORs, where -1s are like 0s (and 1s are still 1s). The expected behavior is listed in `dataset`, and neural networks are assigned a fitness based on how close to the expected behavior they come. After 50 or so generations, or 10 seconds of computation, the networks exhibit the expected behavior.

```elixir
#################
# Training data #
#################
dataset = [{{-1, -1}, -1}, {{1, -1}, 1}, {{-1, 1}, 1}, {{1, 1}, -1}] #{{in1, in2} output} -> the expected behavior


####################
# Fitness function #
####################
fitness = fn ann ->
  sim = Ann.Simulation.new(ann)
  error = Enum.reduce dataset, 0, fn {{in1, in2}, out}, error ->
    inputs = %{:in1 => in1, :in2 => in2, :bias => 1.0}
    evaled_sim = Ann.Simulation.eval(sim, inputs) #Then we'll evaluate a simulation with these given inputs
    result = evaled_sim.data[:out]
    #Then we'll add the distance between the result and the expected output to the accumulated error
    error + abs(result - out)
  end

  #We're supposed to return fitness (where greater values are better) instead of error (where greater values are worse).
  #The maximum our error can be is 8, so we return (8 - error)^2 as our fitness.
  :math.pow(8 - error, 2)
end


######################
# Training the model #
######################
seed_ann = Ann.new([:in1, :in2, :bias], [:out])

DistNeatEx.evolve(seed_ann, fitness, run_id: :xor)

#After it has had time to train, we can retrieve its best output
{ann, fitness} = DistNeatEx.best_ann(:xor)


###################
# Using the model #
###################
#We can then try out the produced ANN ourselves by making a simulation
simulation = ann
|> Ann.Simulation.new
|> Ann.Simulation.eval(%{:in1 => 1.0, :in2 => 1.0, :bias => 1.0})

IO.inspect simulation.data[:out]
```